String.prototype.removeDiacritics = function() {
    var diacritics = [
        [/[\300-\306]/g, 'A'],
        [/[\340-\346]/g, 'a'],
        [/[\310-\313]/g, 'E'],
        [/[\350-\353]/g, 'e'],
        [/[\314-\317]/g, 'I'],
        [/[\354-\357]/g, 'i'],
        [/[\322-\330]/g, 'O'],
        [/[\362-\370]/g, 'o'],
        [/[\331-\334]/g, 'U'],
        [/[\371-\374]/g, 'u'],
        [/[\321]/g, 'N'],
        [/[\361]/g, 'n'],
        [/[\307]/g, 'C'],
        [/[\347]/g, 'c'],
    ];
    var s = this;
    for (var i = 0; i < diacritics.length; i++) {
        s = s.replace(diacritics[i][0], diacritics[i][1]);
    }
    return s;
}

function SelfAssessmentSurveyBlock(runtime, element) {

	function getSurvey() { 
		var handlerUrl = runtime.handlerUrl(element, 'get_survey');
		$.post(handlerUrl, '{}')
			.done(function(response) {
				if(response.success) {
					var assessmentHandleUrl = runtime.handlerUrl(element, 'get_assessment');
                	$.post(assessmentHandleUrl, '{}')
                		.done(function (assessmentResponse) {
                			var selfAssessment = '';
	                        if (assessmentResponse.success) {
	                            selfAssessment = JSON.parse(assessmentResponse['assessment']);
	                        }
							var survey = JSON.parse(response['survey']);
                            var categories = survey.categories
							var meta = survey.meta
							var ratingLabels = survey.rating_labels
							$("#label-title", element).html(meta['title']);
							$("#label-instructions", element).html(meta['instruction_text']);
							//ratingLabels.forEach(function (label, index) {
							//	$("#self-assessment-survey thead tr").append('<th id="label-score-'+index.toString()+'">'+ratingLabels[index].label+'</th>'); 
							//}) 
							var surveyTable = $('#self-assessment-survey-table', element);
                            categories.forEach(function (category, indexCat) {
                                var questions = category.questions
				$("#self-assessment-survey tbody").append("<tr class='self-assessment-category ' id='cat-"+category.name.replace(/ /g,'-').replace(/[';:,]/g,'').removeDiacritics().toLowerCase()+"'><td colspan='"+ (ratingLabels.length + 1) +"'>"+category.name+"</td>");
				$("#self-assessment-survey tbody").append("<tr class='self-assessment-category-header' id='cat-header-"+category.name.replace(/ /g,'-').replace(/[';:,]/g,'').removeDiacritics().toLowerCase()+"'><td colspan='"+ (1) +"'></td>");
    				ratingLabels.forEach(function (label, index) {
                                                        $("#cat-header-"+category.name.replace(/ /g,'-').replace(/[';:,]/g,'').removeDiacritics().toLowerCase()).append('<th id="label-score-'+index.toString()+'">'+ratingLabels[index].label+'</th>');
                                                       })
				
                                questions.forEach(function (question, index) { 
    								$("#self-assessment-survey tbody").append("<tr class='self-assessment-question' id='question-"+question.id+"'><td>"+question.text+"</td></tr>")
    				                ratingLabels.forEach(function (label, colIndex) {
    									$("#self-assessment-survey tbody tr#question-"+question.id).append("<td><input type='radio' id='radio-"+question.id+"-"+colIndex.toString()+"' name='radio-"+question.id+"' data-value='"+ratingLabels[colIndex].score+"'></td>");
    								});  
    								$("#radio-"+question.id+"-0").attr("checked", "checked")
    								if (selfAssessment[question.id] !== undefined) { 
    									$("tr#question-"+question.id + " input[data-value='"+ selfAssessment[question.id] +"']").attr("checked", "checked")
    								}
    							});	
                            });
						});
					} else {
					console.log('error getting survey');
				}
			}); 
	}

    function compareProfiles(a, b) {
        if (a.match < b.match)
            return 1;
        if (a.match > b.match)
            return -1;
        return 0
    }

	function getProfile() {
		var handlerUrl = runtime.handlerUrl(element, 'get_profile');
        $.post(handlerUrl, '{}')
            .done(function (response) {
                if (response.success) {
                	profiles = JSON.parse(response['profile']);
                	profiles.sort(compareProfiles);
                	profiles.forEach(function (profile, index) {
                		$("#self-assessment-profile tbody").append("<tr class='self-assessment-profile' id='profile-"+profile.name+"'><td>"+profile.name+"</td><td>"+((profile.match*100).toFixed(1))+"%</td></tr>")
								
                	});

                    student_responses = JSON.parse(response['student_responses']);
                    student_responses.forEach(function (student_response, index) {
                        $("#self-assessment-responses tbody").append("<tr class='self-assessment-profile' id='profile-"+student_response.question_id+"'><td>"+student_response.question_text+"</td><td>"+student_response.answer_caption+"</td></tr>")
                    });
                }
            });

	}

    function showSurvey() {
    	var handlerUrl = runtime.handlerUrl(element, 'get_profile');
    	$.post(handlerUrl, '{}')
    		.done(function(response) {
    			if(response.success) {
    				$('.self-assessment-profile-form', element).hide();
    				$('.profile-panel', element).show();
       			} else {
       				console.log('No Profile');
    				$('.self-assessment-profile-form', element).show();
    				$('.profile-panel', element).hide();
    			}
    		})
    }

    $('.save-button', element).click(function(eventObject) {
        submitSurvey();
    });

    //$('.save-button', element).click(function(eventObject) {
    var submitSurvey = function() {
    	var questions = $("tr.self-assessment-question");
    	var handlerUrl = runtime.handlerUrl(element, 'student_save_response');
    	var data = {data:{}};
    	//questions.forEach(function (question, index) {
    	//$("tr.self-assessment-question").each(function( index ) { 
    	$.each(questions, function( index, question ) { 
    		var questionId = $(this).attr('id').substring(9);
    		var selectedVal = "";
			var selected = $("input[type='radio'][name='radio-" + questionId + "']:checked"); 
			selectedVal = selected.val();
    		//var q = {}
    		data.data[questionId] = parseInt(selected.attr('data-value'));
    		//data.data.push(q);
    	}); 
        $.ajax({
    		type: "POST", 
    		url : handlerUrl,
                async : false,
                data: JSON.stringify(data)
        }).done(function()  {
        	showSurvey();		
	}).fail(function()  {
    		alert("Failed to save. ");
   	});
        //console.log(JSON.stringify(data)); 
        //$.post(handlerUrl, JSON.stringify(data))
        	//.done(function (resp) {
        		//if (resp) { 
        	//		showSurvey();
        		//} else {
        		//	runtime.notify('error',  {
                    	//	title: 'Error : save failed.',
                    	//	message: 'An error occured !'
                	//	});

        //		}
                //)
                //.fail(function (resp) {
                //     console.log('save failed'); 
                //});
     
    };

    $('.reset-survey-btn', element).on('click', function () {
        var handlerUrl = runtime.handlerUrl(element, 'reset_survey');
        $.post(handlerUrl, '{}')
            .done(function () {
                $('select', element).val([]);
                showSurvey();
            });
    });

    $(function ($) {
        /* Here's where you'd do things on page load. */
        getSurvey();
        getProfile();
        showSurvey();
    });
}	
