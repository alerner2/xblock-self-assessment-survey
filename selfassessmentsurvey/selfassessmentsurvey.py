# coding=utf-8
import pkg_resources
import requests
import json

from urlparse import urlparse

from django.template import Context, Template

from xblock.core import XBlock
from xblock.fields import Scope, Integer, String
from xblock.fragment import Fragment

from .surveydefaults import DEFAULT_SURVEY


class SelfAssessmentSurveyBlock(XBlock):

    display_name = String(help="This name appears in horizontal navigation at the top of the page.",
                          default="Self Assessment Survey",
                          scope=Scope.content
    )

    profile = String(
        scope = Scope.user_state,
        help='Student Profile'
    )

    survey = String(
        scope=Scope.content,
        help='Self Assessment Survey Questions'
    )

    assessment = String(
        scope=Scope.user_state,
        help='Self Assessment Survey Responses'
    )

    def student_view(self, context=None):
        survey_json = DEFAULT_SURVEY if not self.survey else self.survey

        print(survey_json)
        survey = json.loads(survey_json)

        context = {
            'success': True,
            'survey_title': 'My Survey'
        }
        
        html = self.render_template('static/html/survey.html', context)
        frag = Fragment(html.format(self = self))

        frag.add_css(self.resource_string('static/css/selfassessmentsurvey.css'))
        frag.add_javascript(self.resource_string('static/js/selfassessmentsurvey-view.js'))
        frag.initialize_js('SelfAssessmentSurveyBlock')
        return frag

    def studio_view(self, context=None):
        context = {
            'success': True,
            'survey': DEFAULT_SURVEY if not self.survey else self.survey
        }
        html = self.render_template('static/html/survey-edit.html', context)
        frag = Fragment(html.format(self = self))
 
        frag.add_javascript(self.resource_string('static/js/selfassessmentsurvey-edit.js'))
        frag.initialize_js('SelfAssessmentSurveyBlockStudio')
        return frag

    def get_question_text(self, questionId):
        survey = json.loads(self.survey)
        question_text = 'NOT FOUND'
        for category in survey['categories']:
            for question in category['questions']:
                if question['id'] == questionId:
                    question_text = question['text']
                    break

        return question_text        

    def get_answer_caption(self, score):
        survey = json.loads(self.survey)
        answer_caption = 'NOT FOUND'
        for rating_label in survey['rating_labels']:
            if rating_label['score'] == score:
                answer_caption = rating_label['label']
                break

        return answer_caption

    def get_question_pos(self, questionId):
        '''
        We should rewrite this. After a survey is submitted, the student
        can see the answers submitted. We don't categorize them, as categories
        were added as an after thought.
        This returns the position of the question based on the questionId 
        '''
        survey = json.loads(self.survey)
        question_pos = -1
        current_pos = -1
        for category in survey['categories']:
            for question in category['questions']:
                current_pos = current_pos + 1
                if question['id'] == questionId:
                    question_pos = current_pos
                    break

        return question_pos

    def is_json(self, myjson):
        try:
            json.loads(myjson)
        except ValueError, e:
            print e
            return False
        return True

    def init_survey(self, data):
        survey_json = json.loads(data)
        self.survey = json.dumps(survey_json)


    @XBlock.json_handler
    def get_survey(self, data, suffix=''):
        #survey = json.loads(self.survey)
        if not self.survey:
            self.init_survey(DEFAULT_SURVEY)

        return {
            'success': True,
            'survey': self.survey
        }

    @XBlock.json_handler
    def reset_survey(self, data, success=''):
        self.assessment = None
        self.profile = None    
    
        return {'success': True}

    @XBlock.json_handler
    def get_profile(self, data, success=''):
        if not self.profile:
            return {'success': False}
        else:
            student_responses = []
            assessment = json.loads(self.assessment)
            
            for q in sorted(assessment.items(), key=lambda i:self.get_question_pos(i[0])):
                student_response = {}
                student_response['question_id'] = q[0]
                student_response['question_text'] = self.get_question_text(q[0])
                student_response['answer_value'] = q[1]
                student_response['answer_caption'] = self.get_answer_caption(q[1])
                student_responses.append(student_response)

            return {'success': True, 'profile': self.profile, 'student_responses': json.dumps(student_responses)}


    def render_template(self, template_path, context={}):
        '''
        Evaluate a template by resource path, applying the provided context
        '''
        template_str = self.load_resource(template_path)
        return Template(template_str).render(Context(context))

    def calc_profile(self):
        survey = json.loads(self.survey)
        assessment = json.loads(self.assessment)
        profiles = []
        for profile_def in survey['profile_definitions']:
            profile = {'name':'', 'match': 0}
            print(profile_def['name'].encode('utf-8'))
            profile['name'] = profile_def['name']
            
            total_count = 0
            match_count = 0
            for key, value in profile_def['targets'].iteritems():
                total_count = total_count + 1
                if assessment[key] == value:
                    match_count = match_count + 1
            print str(match_count) + ' ' + str(total_count)
            profile['match'] = match_count/float(total_count)
            profiles.append(profile)
        return(profiles)
        #return({"profiles": [{"name": "Profile 1", "match": .40 }, {"name": "Profile 2", "match": .40 }, {"name": "Profile 3", "match": .10 }]})

    @XBlock.json_handler
    def student_save_response(self, data, suffix=''):
        event_type = 'learning.course.selfassessment.submit'
        self.assessment = json.dumps(data['data'])
        self.profile = json.dumps(self.calc_profile())
        event_data = {
            'student_assessment': self.assessment,
            'student_profile': self.profile
        }
        self.runtime.publish(self, event_type, event_data)

        return {
            'success': True,
            'profile': self.profile
        }

    @XBlock.json_handler
    def studio_save_response(self, data, suffox=''):
        if self.is_json(data['survey']):
            self.init_survey(data['survey'])
            return {
                'survey': self.survey,
                'success': True
            }
        else:
            return {
                'success': False,
                'errors': 'Invalid JSON'
            }          

    @XBlock.json_handler
    def get_assessment(self, data, suffix=''):
        """
        Return student's assessment in json.
        """
        if self.assessment is not None:
            return {
                'success': True,
                'assessment': self.assessment
            }
        else:
            return {
                'success': False 
            }

    
            
    @staticmethod
    def workbench_scenarios():
        """A canned scenario for display in the workbench."""
        return [
            ("self assessment survey",
            """
            <vertical_demo>
                <selfassessmentsurvey />
            </vertical_demo>
            """)
        ]  

    def load_resource(self, resource_path):
        '''
	    Gets the content of a resource
        '''
        resource_content = pkg_resources.resource_string(__name__, resource_path)
        return resource_content.decode('utf8')

    def resource_string(self, path):
        '''Handy helper for getting resources from our kit.'''
        data = pkg_resources.resource_string(__name__, path)
        return data.decode('utf8')    

 
